import { initializeApp, credential, app } from 'firebase-admin'
import { firestore } from 'firebase-admin/lib/firestore';

export class FirestoreService<T> {
    private app: app.App;
    private collection: firestore.CollectionReference<T>;

    public db: firestore.Firestore;

    constructor(collection: string) {
        if (process.env.ENV === 'prod' || process.env.ENV === 'docker') {
            this.app = initializeApp({
                credential: credential.applicationDefault(),
                projectId: 'monitor-vacina-covid'
            });
            this.app.instanceId();
        } else {
            this.app = initializeApp({
                credential: credential.cert('pushuser.json'),
                projectId: 'monitor-vacina-covid'
            })
        }
        

        this.db = this.app.firestore();

        this.collection = this.db.collection(collection) as firestore.CollectionReference<T>;
    }

    public getCollection(collection: string) {
        return this.db.collection(collection) as firestore.CollectionReference<T>;
    }

    public setDefaultCollection(collection: string) {
        this.collection = this.getCollection(collection)
    }

    public set(key: string, value: T) {
        return this.collection.doc(key).set(value);
    }

    public get(doc: string) {
        return this.collection.doc(doc);
    }

    public getAll() {
        return this.collection.get();
    }

    public delete(doc: string) {
        return this.collection.doc(doc).delete();
    }
}