import webpush from 'web-push';
import { VAPID_PRIVATE, VAPID_PUBLIC } from '../environment';
import { Dose } from '../interfaces/grupo.interface';
import { LocalGrupo } from '../interfaces/localGrupo.interface';
import { UserSubscription } from '../models/user_subscription.model';

export class PushService {
    constructor(
    ) {
        webpush.setVapidDetails('mailto:douglass.sousa@outlook.com.br', VAPID_PUBLIC, VAPID_PRIVATE)
    }

    public async sendToUser(userSubscription: UserSubscription, localGrupoArray: LocalGrupo[]) {
        localGrupoArray.filter((lgd: LocalGrupo) => userSubscription.tipos.some(t => t == lgd.grupo.tipo) && userSubscription.doses.some(d => d === lgd.grupo.dose)).forEach(lg => {
            const notificationPayload = {
                action: 'http://gti.serra.es.gov.br/saude/',
                notification: {
                    title: lg.local.local,
                    body: `${lg.grupo.texto} - ${lg.grupo.dose == Dose.primeira ? '1ª Dose' : '2ª Dose'}`,
                    icon: '/assets/svg/seringa.svg'
                }
            }

            webpush.sendNotification(
                userSubscription.subscription,
                JSON.stringify(notificationPayload)
            ).catch(error => {
                throw error
            });
            
        })
    }
}