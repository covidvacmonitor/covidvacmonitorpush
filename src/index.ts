import { PushService } from './services/push.service';
import axios, { AxiosResponse } from "axios"
import { Retorno } from "./interfaces/retorno.interface";
import { FirestoreService } from './services/firestore.service';
import { UserSubscription } from './models/user_subscription.model';
import { API_URL } from './environment';

const pushService = new PushService();
const firestoreService = new FirestoreService<UserSubscription>('subscriptions');

export const enviarNotificacoes = async (message: any, context: any) => {
    // try
    const { data } = await axios.get<Retorno>(API_URL, {timeout: 30000});

    if (data.data.length > 0) {
        const subscriptions = await firestoreService.getAll();

        subscriptions.query.where("subscription", "!=", null).get()
            .then(result => {
                return Promise.all(result.docs.map(async userSub => pushService.sendToUser(userSub.data(), data.data)))
            })
            .then(() => {
                if (subscriptions.size > 0) process.stdout.write('Todas notificações enviadas com sucesso!');
            })
            .catch(error => {
                process.stderr.write(`Erro ao enviar notificações: ${JSON.stringify(error)}`);
            })
        // Promise.all(subscriptions.docs.map(async userSub => pushService.sendToUser(userSub.data(), data.data)))
        //     .then(() => {
        //         if (subscriptions.size > 0) process.stdout.write('Todas notificações enviadas com sucesso!');
        //     })
        //     .catch(error => {
        //         process.stderr.write(`Erro ao enviar notificações: ${JSON.stringify(error)}`);
        //     })
    }
}