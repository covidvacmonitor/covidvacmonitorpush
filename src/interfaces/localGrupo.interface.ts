import { Grupo } from "./grupo.interface";
import { Local } from "./local.interface";

export interface LocalGrupo {
  local: Local;
  grupo: Grupo
}
