FROM node:14-slim as build
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci
COPY src ./src
COPY tsconfig.json ./tsconfig.json
RUN npm run build

FROM node:14-slim as serve
WORKDIR /usr/src/app
COPY --from=build /usr/src/app/build ./
RUN npm ci --only=production
CMD ["npm", "run", "start:docker"]